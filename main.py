from selenium import webdriver
import csv
import time
import os

with open('bettingodds.csv', 'w', newline='') as f:
    thewriter = csv.writer(f)
    thewriter.writerow(["TEAMS","SPREAD","SPREAD ODDS","","MONEY LINE","", "O/U", "O/U ODDS"])
    thewriter.writerow("")

web = 'https://www.williamhill.com/us/az/bet/americanfootball' #you can choose any other league

driver = webdriver.Chrome()
driver.get(web)

time.sleep(1)
SCROLL_PAUSE_TIME = 0

# Get scroll height
last_height = driver.execute_script("return document.body.scrollHeight")

while True:
    # Scroll down to bottom
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load page
    time.sleep(SCROLL_PAUSE_TIME)

    # Calculate new scroll height and compare with last scroll height
    new_height = driver.execute_script("return document.body.scrollHeight")
    if new_height == last_height:
        break
    last_height = new_height

sport_title = driver.find_elements("class name", 'title')
for sport in sport_title:
    if sport.text == 'NFL':

        parent = sport.find_element("xpath", './..')
        grandparent = parent.find_element("xpath", './..')
        single_row_events = grandparent.find_elements("class name", 'EventCard')
        for i in single_row_events:
            name = f"{i.text}"
            namerrrr = name.split('\n')
            if "SPREAD LIVE" not in namerrrr:
                namerrrr.remove("SPREAD")
                namerrrr.remove("MONEY LINE")
                namerrrr.remove("TOTAL POINTS")
                with open('bettingodds.csv', 'a', newline='') as f:
                    thewriter = csv.writer(f)
                    thewriter.writerow([namerrrr[1], namerrrr[3],namerrrr[4],"",namerrrr[7],"",namerrrr[9],namerrrr[10]])
                    thewriter.writerow([namerrrr[2], namerrrr[5],namerrrr[6],"",namerrrr[8],"",namerrrr[11],namerrrr[12]])
                    thewriter.writerow("")

os.system("start EXCEL.EXE bettingodds.csv")
